# meta-aikaan

Container layer for yocto project to include aikaan agent

Layers:
mata-aikaan-core: Provides recipes for aikaan agent and device registration credentials.

How to use:
* Make sure that you have a valid account in aikaan controller
* Set up yocto develop build directory
* Clone this repository and checkout the appropriate branch.
* Run ./setup-account.sh to fetch the registration credentials
* Add the meta-aikaan/meta-aikaan-core into build/bblayers.conf file
* Build with bitbake as: bitbake core-image-minimal
   To compile for other image; copy meta-aikaan/recipes-image/core-image-minimal.bbappend
* Refer templates/local.conf.template if you want to configure installation directories



