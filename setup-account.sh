#!/bin/bash
set -e

config_recipe=meta-aikaan-core/recipes-aikaan/aikaan-account
config_tar_dir=$config_recipe/files
config_tar_file=$config_tar_dir/aikaan_config.tar
bb_file=$config_recipe/aikaan-account.bb

# ERR code must be 0-255
JQ_ERR=254
CONT_LOGIN_ERR=252
CONT_DGP_ERR=250


compatibility_check() {
    #Check if jq available in the PATH
    util=jq
    type $util

    if [ $? -ne 0 ]; then
        echo "This scripts requires $util'; but couldn't find it in available PATH"
        echo "Please install $util (sudo apt install -y $util) and try again"
        echo "Exiting with err: $JQ_ERR"
        exit $JQ_ERR
    fi
}

login_and_get_cookie() {
req_body=$(cat << EOF
{"email":"$aikaan_controller_email", "password":"$aikaan_controller_password"}
EOF
)

    resp=$(curl -s -k $LOGIN_API -d "$req_body")
    if [ $? -ne 0 ]; then
        echo "Login error: Please check your credentials and controller URL"
        exit 1
    fi

    userID=$(echo $resp | jq -r .data.email)
    JWT=$(echo $resp | jq -r .data.token)
    cookieArg=$( printf 'aicon-jwt=%s' "$JWT" )
}

profileName2Id() {
   # Get the device profile id from name
   cmd=dgpname2id
   req_body=$(cat << EOF
   {"search":"$aikaan_controller_device_profile_name","sort":{"key":0,"order":0}}
EOF
   )
   DGP_API="${aikaan_controller_url}/um/api/dgp/v1/dgpsearch?limit=10&offset=0"
   resp=$(curl -skq --cookie $cookieArg "${DGP_API}" -d "$req_body")
   #resp=$(curl -s -k $DGP_API)
   if [ $? -ne 0 ]; then
       echo "DGP access error: Please check your credentials and controller URL"
       exit 1
   fi
   profileID=$(echo $resp | jq -r .profiles[0].profileid)
}

agent_config_download() {
   CONFIG_API="${aikaan_controller_url}/api/_external/img/v1/dgp/${profileID}/deviceconf"
   curl -skq --cookie $cookieArg "${CONFIG_API}" -o "$config_tar_file"
   status=$?
   if [ $status -ne 0 ]; then
	   echo "DGP access error($status): Please check your credentials and controller URL"
       exit 1
   fi

   # Update bb file with checksum
   chksum=$(sha256sum --binary $config_tar_file | awk '{print $1}')
   echo "Checksum: $chksum"
   #sed -i -e "s/AIKAAN_CONFIG_TAR_SHA256SUM/$chksum/g" $bb_file
   sed -i -e "s:SRC_URI\[sha256sum\].*$:SRC_URI\[sha256sum\] = \"$chksum\":g" $bb_file
}

############################################################
#                MAIN
############################################################
echo "This is a one-time task to setup based on user account"
read -p "Enter any key to preceed: "

compatibility_check
mkdir -p $config_tar_dir

# Check if the file exists already
if [ -f $config_tar_file ]; then
    read -p "$config_tar_file already present. Press any key to overwrite: "
    # backup
    mv $config_tar_file $config_tar_file.$$
fi

read -p "Controller URL:- [ex: https://experience.aikaan.io]:-     https://" controller_url
aikaan_controller_url=https://${controller_url}
read -p "Controller:- [$aikaan_controller_url] login Credentials:- email-id:  " aikaan_controller_email
read -p "Controller:- [$aikaan_controller_url] login Credentials:- password:  " aikaan_controller_password
read -p "Name of Device Profile under which device should register: " aikaan_controller_device_profile_name

LOGIN_API=${aikaan_controller_url}/api/_external/auth/v1/signin

clear
echo "Your credentials:"
echo "    controller URL [$aikaan_controller_url]"
echo "    email id [$aikaan_controller_email]"
echo "    password [$aikaan_controller_password]"
echo "    device profile [$aikaan_controller_device_profile_name]"

read -p "Enter any key to preceed: "

echo "Connecting to controller; please wait ..."
login_and_get_cookie

profileName2Id
echo "Devide Profile Name: $aikaan_controller_device_profile_name ID: $profileID"

echo "Downloading configurations to $config_tar_file"
agent_config_download

# TODO: test the persence of the file and check whether it is a valid tar or not
tar -t -f $config_tar_file
if [ $? -ne 0 ]; then
    echo "$config_tar_file is corrupted .... Please download it manually"
    exit 1
fi

echo "Configurations saved as:  $config_tar_file"
