require aikaan-agent.inc
AIKAAN_AGENT_VERSION = "2.48.1"

SRC_URI_append_arm = " ${AIKAAN_AGENT_IMAGE_DIR}/aiagent-${AIKAAN_AGENT_VERSION}-arm.tar.gz;name=arm"
SRC_URI[arm.sha256sum] = "194b4f8e756a9993e6fa3eec003f08d5d635d80f0aedd7ba3fe8c0f34b4ea0b6"

SRC_URI_append_x86-64 = " ${AIKAAN_AGENT_IMAGE_DIR}/aiagent-${AIKAAN_AGENT_VERSION}-amd64.tar.gz;name=x86-64"
SRC_URI[x86-64.sha256sum] = "77dd0e4ffd891b883eae91940333933298e13712e9c3f83a9a6476dfd08fdb35"

SRC_URI_append_i586 = " ${AIKAAN_AGENT_IMAGE_DIR}/aiagent-${AIKAAN_AGENT_VERSION}-386.tar.gz;name=i586"
SRC_URI[i586.sha256sum] = "c5aae3f76d1488d7d98da49af72d1d51f964891768d55a5669ce3b1fe416af6f"

SRC_URI_append_i686 = " ${AIKAAN_AGENT_IMAGE_DIR}/aiagent-${AIKAAN_AGENT_VERSION}-386.tar.gz;name=i686"
SRC_URI[i686.sha256sum] = "c5aae3f76d1488d7d98da49af72d1d51f964891768d55a5669ce3b1fe416af6f"
