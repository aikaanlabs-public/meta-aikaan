DESCRIPTION = "aikaan agent recipe for yocto build"
LICENSE = "CLOSED"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
AIKAAN_AGENT_INSTALL_DIR ?= "/"
AIKAAN_AGENT_CONFIG_DIR ?= "${AIKAAN_AGENT_INSTALL_DIR}"
AIKAAN_AGENT_IMAGE_URL ?= "https://packages.aikaan.io"
AIKAAN_AGENT_IMAGE_DIR ?= "${AIKAAN_AGENT_IMAGE_URL}/yocto/aiagent"

SRC_URI += "${AIKAAN_AGENT_IMAGE_URL}/scripts/aikaan.sysvinit.template;name=sysvinit"
SRC_URI[sysvinit.sha256sum] = "5dd4dabb17eede92b1d88671cb004c08ca95dbbddacdd839b23d232a20c21cd5"
SRC_URI += "${AIKAAN_AGENT_IMAGE_URL}/scripts/aikaan.systemd.template;name=systemd"
SRC_URI[systemd.sha256sum] = "60a4ff84f7d3c638781b459c920c210b863b26472bc5dfd0feb0079bb923abfd"

S = "${WORKDIR}"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

INITSCRIPT_NAME = "aikaan"
INITSCRIPT_PARAMS = "defaults"
#INITSCRIPT_PARAMS = "start 99 5 2 . stop 20 0 1 6 ."

inherit update-rc.d systemd

do_install() {
    # Create /usr/bin folder in the rootfs with default permission
    install -d ${D}${base_prefix}${AIKAAN_AGENT_INSTALL_DIR}
    # Install the application into that directory
    cp --no-preserve=ownership --preserve=mode,timestamps -r ${WORKDIR}/opt ${D}${base_prefix}${AIKAAN_AGENT_INSTALL_DIR}/

    # Install init
    envstr="\"AIKAAN_AGENT_INSTALL_DIR=${AIKAAN_AGENT_INSTALL_DIR}\" \"AIKAAN_AGENT_CONFIG_DIR=${AIKAAN_AGENT_CONFIG_DIR}\""
    if ${@bb.utils.contains('DISTRO_FEATURES', 'systemd', 'true', 'false', d)}; then
        install -d ${D}${systemd_system_unitdir}
        install -m 0644 ${WORKDIR}/aikaan.systemd.template ${D}${systemd_system_unitdir}/aikaan.service
        sed -i -e "s|__AIKAAN_ENV_STRING__|$envstr|g" ${D}/${systemd_system_unitdir}/aikaan.service
    elif ${@bb.utils.contains('DISTRO_FEATURES', 'sysvinit', 'true', 'false', d)}; then
        install -d ${D}${sysconfdir}/init.d/
        install -c -m 755 ${WORKDIR}/aikaan.sysvinit.template ${D}${sysconfdir}/init.d/${INITSCRIPT_NAME}
        sed -i -e "s|__AIKAAN_ENV_STRING__|$envstr|g" ${D}${sysconfdir}/init.d/${INITSCRIPT_NAME}
    fi
}

do_install_append() {
    install -d ${D}${base_prefix}/system/bin/
    ln -s /bin/sh ${D}${base_prefix}/system/bin/sh
}

FILES_${PN} += "${base_prefix}${AIKAAN_AGENT_INSTALL_DIR}/*"
FILES_${PN} += "${systemd_system_unitdir}/aikaan.service"
FILES_${PN} += "${base_prefix}/system/bin/sh"
SYSTEMD_SERVICE_${PN} = "aikaan.service"

INSANE_SKIP_${PN} += "already-stripped"
# Masking error for '/system/bin/sh' reference in some files;
INSANE_SKIP_${PN} += "file-rdeps"
# Masking error for '//', incase of empty variables
INSANE_SKIP_${PN} += "files-invalid"
INSANE_SKIP_${PN} += "ldflags"
