DESCRIPTION = "aikaan account information for agent registration"
LICENSE = "CLOSED"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
AIKAAN_AGENT_INSTALL_DIR ?= "/"
AIKAAN_AGENT_CONFIG_DIR ?= "${AIKAAN_AGENT_INSTALL_DIR}"

SRC_URI = "file://aikaan_config.tar;unpack=0"
SRC_URI[sha256sum] = "933b9725ab772c5220cf0f5601b810e1cc9cf505783aed0130a9c7ea20ae4467"

S = "${WORKDIR}"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_compile() {
}

do_install() {
    install -d ${D}${base_prefix}${AIKAAN_AGENT_INSTALL_DIR}/opt/aikaan/etc
    tar --no-same-owner -xf ${WORKDIR}/aikaan_config.tar -C ${D}${base_prefix}${AIKAAN_AGENT_INSTALL_DIR}/opt/aikaan/etc/
}

FILES_${PN} += "${base_prefix}${AIKAAN_AGENT_INSTALL_DIR}/*"

# Masking error for '//', incase of empty variables
INSANE_SKIP_${PN} += "files-invalid"
